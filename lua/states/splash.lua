-- chikun :: 2014-2015
-- Splash state


-- Temporary state, removed at end of script
local SplashState = class(PrototypeClass, function(new_class) end)


-- On state create
function SplashState:create()

	self.has_beeped = false
	self.timer = 0

	lg.setBackgroundColor(0, 0, 0)
end


-- On state update
function SplashState:update(dt)

	self.timer = self.timer + dt

	if (self.timer >= 0.5 and not has_beeped) then

		sfx.startup:play()

		has_beeped = true
	end

	if (self.timer > 1) then

		cs:change("template")
	end
end


-- On state draw
function SplashState:draw()

	local alpha = 1 - math.abs(self.timer - 0.5) * 2

	lg.setColor(255, 255, 255, alpha * 255)
	lg.setFont(cf.splash)

	lg.printf("chikun", 0,
	          (GAME_HEIGHT - cf.splash:getHeight()) / 2, GAME_WIDTH, 'center')
end


-- On state kill
function SplashState:kill() end


-- Transfer data to state loading script
return SplashState
